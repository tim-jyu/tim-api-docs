.. TIM documentation master file, created by
   sphinx-quickstart on Fri Jun  9 10:36:12 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to TIM's documentation!
===============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   docparagraph
   TIM Python API <modules>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
