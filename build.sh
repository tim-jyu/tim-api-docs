#!/usr/bin/env bash

# check if tim_src folder exists
if [ ! -d "tim_src" ]; then
    echo "tim_src folder does not exist, cloning tim_src"
    git clone https://gitlab.com/tim-jyu/tim.git tim_src
    (
        cd tim_src || exit
        cp variables.sh.template variables.sh
        chmod +x variables.sh
        sed -i 's/echo variables.sh/#echo variables.sh/' variables.sh
    )
else
    echo "tim_src folder exists, pulling tim_src"
    (
        cd tim_src || exit
        git pull
    )
fi

(
    echo "Pulling latest TIM Docker image"
    cd tim_src || exit
    ./dc pull --quiet tim
)

tim_image_tag=$(tim_src/get_latest_date.sh)
echo "Latest TIM Docker image tag: $tim_image_tag"

echo "Building docs"
docker run --rm -v "$(pwd):/service" "timimages/tim:$tim_image_tag" bash -c "\
    pip install sphinx-book-theme && \
    sphinx-apidoc -o source tim_src && \
    sphinx-build source public
"